<?php namespace Viamage\WebMonitor\Console;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use App;
use Viamage\WebMonitor\Classes\CertificateChecker;
use Viamage\WebMonitor\Classes\WebsiteChecker;
use Viamage\WebMonitor\Contracts\WebsiteRepository;
use Viamage\WebMonitor\Repositories\WebsiteEloquentRepository;

class SslCheck extends Command
{
    /**
     * @var string The console command name.
     */
    protected $name = 'webmonitor:sslcheck';

    /**
     * @var string The console command description.
     */
    protected $description = 'No description provided yet...';

    /**
     * Execute the console command.
     * @return void
     */
    public function handle()
    {
        /** @var WebsiteEloquentRepository $repo */
        $repo = App::make(WebsiteRepository::class);

        $websites = $repo->getForSSLCheck();
        foreach($websites as $website){
            $checker = new CertificateChecker($website);
            $days  = $checker->check();
            $message = $website->url.' certificate will expire in '.$days.' days';
            if($days > 7) {
                $this->info($message);
            } else {
                $this->warn($message);
            }

        }
    }

    /**
     * Get the console command arguments.
     * @return array
     */
    protected function getArguments()
    {
        return [];
    }

    /**
     * Get the console command options.
     * @return array
     */
    protected function getOptions()
    {
        return [];
    }
}
