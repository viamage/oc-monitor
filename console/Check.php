<?php namespace Viamage\WebMonitor\Console;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use App;
use Viamage\WebMonitor\Classes\WebsiteChecker;
use Viamage\WebMonitor\Contracts\WebsiteRepository;
use Viamage\WebMonitor\Repositories\WebsiteEloquentRepository;

class Check extends Command
{
    /**
     * @var string The console command name.
     */
    protected $name = 'webmonitor:check';

    /**
     * @var string The console command description.
     */
    protected $description = 'No description provided yet...';

    /**
     * Execute the console command.
     * @return void
     */
    public function handle()
    {
        /** @var WebsiteEloquentRepository $repo */
        $repo = App::make(WebsiteRepository::class);

        $websites = $repo->getActive();
        foreach($websites as $website){
            $checker = new WebsiteChecker($website);
            $up = $checker->check();
            if($up){
                $this->info($website->url . ' is up');
            } else {
                $this->warn($website->url . ' is down');
            }
        }
    }

    /**
     * Get the console command arguments.
     * @return array
     */
    protected function getArguments()
    {
        return [];
    }

    /**
     * Get the console command options.
     * @return array
     */
    protected function getOptions()
    {
        return [];
    }
}
