<?php namespace Viamage\WebMonitor\Models;

use Model;

/**
 * WebsiteLog Model
 * @property             $id
 * @property             $website_id
 * @property             $is_up
 * @property Website     $website
 * @property             $code
 * @property string|null $error
 * @property             $created_at
 * @property             $updated_at
 */
class WebsiteLog extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'viamage_webmonitor_website_logs';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $belongsTo = [
        'website' => Website::class,
    ];
}
