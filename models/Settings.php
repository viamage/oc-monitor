<?php

namespace Viamage\WebMonitor\Models;

use October\Rain\Database\Model;


/**
 * Class Settings
 *
 * @package Keios\SlackNotifications
 */
class Settings extends Model
{

    /**
     * @var array
     */
    public $implement = ['System.Behaviors.SettingsModel'];

    /**
     * @var string
     */
    public $settingsCode = 'v_webmonitor_settings';

    /**
     * @var string
     */
    public $settingsFields = 'fields.yaml';

}
