<?php namespace Viamage\WebMonitor\Models;

use Cache;
use Carbon\Carbon;
use Model;
use October\Rain\Database\Collection;
use Spatie\SslCertificate\SslCertificate;

/**
 * Website Model
 * @property                         $id
 * @property                         $url
 * @property                         $interval
 * @property Collection|WebsiteLog[] $logs
 * @property                         $is_enabled
 * @property                         $check_ssl
 * @property                         $cert_days
 * @property                         $created_at
 * @property                         $updated_at
 * @property int                     $check_from
 * @property int                     $check_to
 *
 */
class Website extends Model
{
    public const STATE_DISABLED = 'NOT MONITORED';
    public const STATE_UNKNOWN  = 'UNKNOWN';
    public const STATE_UP       = 'UP';
    public const STATE_DOWN     = 'DOWN';

    /**
     * @var string The database table used by the model.
     */
    public $table = 'viamage_webmonitor_websites';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasMany = [
        'logs' => WebsiteLog::class,
    ];

    public function afterSave()
    {
        Cache::forget('v_websites');
        Cache::forget('vssl_websites');
    }

    public function getStatus(): string
    {
        if (!$this->is_enabled) {
            return self::STATE_DISABLED;
        }
        $lastLog = $this->logs->sortByDesc('created_at')->first();
        if (!$lastLog) {
            return self::STATE_UNKNOWN;
        }
        if ($lastLog->is_up) {
            return self::STATE_UP;
        }

        return self::STATE_DOWN;
    }

    public function getStatusMessage(): string
    {
        if (!$this->is_enabled) {
            return 'NOT MONITORED';
        }
        $lastLog = $this->logs->sortByDesc('created_at')->first();
        if (!$lastLog) {
            return '<strong>Unknown Status</strong>';
        }
        if ($lastLog->is_up) {
            return 'UP';
        }
        $now = Carbon::now();

        $msg = '<strong>DOWN for '.$now->diffInMinutes($lastLog->created_at).' minutes</strong>';
        if ($lastLog->error) {
            $msg .= ' <br /><small>'.$lastLog->error.'</small>';
        }

        return $msg;
    }

    public function getDowntimeSince(Carbon $date): int
    {
        $failLog = $this->logs->where('is_up', false)->sortByDesc('created_at')->first();

        return $date->diffInMinutes($failLog->created_at);
    }

    public function getCertificateExpiryDays()
    {
        $certificate = SslCertificate::createForHostName(str_replace(['http://', 'https://'], '', $this->url));

        return $certificate->expirationDate()->diffInDays(); // returns an int
    }
}
