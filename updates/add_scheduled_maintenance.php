<?php namespace Viamage\WebMonitor\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class AddScheduledMaintenance extends Migration
{
    public function up()
    {
        Schema::table(
            'viamage_webmonitor_websites',
            function (Blueprint $table) {
                $table->integer('check_from')->after('is_enabled')->default(0);
                $table->integer('check_to')->after('is_enabled')->default(2359);
            }
        );
    }

    public function down()
    {
        Schema::table(
            'viamage_webmonitor_websites',
            function (Blueprint $table) {
                $table->dropColumn('check_from');
                $table->dropColumn('check_to');
            }
        );
    }
}
