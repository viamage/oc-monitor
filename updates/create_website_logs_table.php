<?php namespace Viamage\WebMonitor\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateWebsiteLogsTable extends Migration
{
    public function up()
    {
        Schema::create('viamage_webmonitor_website_logs', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('website_id')->unsigned();
            $table->integer('code')->default(200);
            $table->boolean('is_up')->default(false);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('viamage_webmonitor_website_logs');
    }
}
