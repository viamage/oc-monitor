<?php namespace Viamage\WebMonitor\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateWebsitesTable extends Migration
{
    public function up()
    {
        Schema::create('viamage_webmonitor_websites', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('url', 255);
            $table->boolean('is_enabled')->default(false);
            $table->integer('interval')->default(5);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('viamage_webmonitor_websites');
    }
}
