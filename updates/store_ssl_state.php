<?php namespace Viamage\WebMonitor\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class StoreSslState extends Migration
{
    public function up()
    {
        Schema::table(
            'viamage_webmonitor_websites',
            static function (Blueprint $table) {
                $table->integer('cert_days')->after('check_ssl')->default(0);
            }
        );
    }

    public function down()
    {
        Schema::table(
            'viamage_webmonitor_websites',
            function (Blueprint $table) {
                $table->dropColumn('cert_days');
            }
        );
    }
}
