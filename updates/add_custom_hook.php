<?php namespace Viamage\WebMonitor\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class AddCustomHook extends Migration
{
    public function up()
    {
        Schema::table(
            'viamage_webmonitor_websites',
            function (Blueprint $table) {
                $table->text('custom_hook')->after('check_ssl')->nullable();

            }
        );
    }

    public function down()
    {
        Schema::table(
            'viamage_webmonitor_websites',
            function (Blueprint $table) {
                $table->dropColumn('custom_hook');
            }
        );
    }
}
