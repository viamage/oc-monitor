<?php namespace Viamage\WebMonitor\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class AddSslCheck extends Migration
{
    public function up()
    {
        Schema::table(
            'viamage_webmonitor_websites',
            function (Blueprint $table) {
                $table->boolean('check_ssl')->after('is_enabled')->default(false);

            }
        );
    }

    public function down()
    {
        Schema::table(
            'viamage_webmonitor_websites',
            function (Blueprint $table) {
                $table->dropColumn('check_ssl');
            }
        );
    }
}
