<?php namespace Viamage\WebMonitor\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class AddErrorDetails extends Migration
{
    public function up()
    {
        Schema::table('viamage_webmonitor_website_logs', function (Blueprint $table) {
            $table->text('error')->after('code')->nullable();

        });
    }

    public function down()
    {
        Schema::table('viamage_webmonitor_website_logs', function (Blueprint $table) {
            $table->dropColumn('error');
        });
    }
}
