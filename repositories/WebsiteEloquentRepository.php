<?php

namespace Viamage\WebMonitor\Repositories;

use Viamage\WebMonitor\Contracts\WebsiteRepository;
use Viamage\WebMonitor\Models\Website;

class WebsiteEloquentRepository implements WebsiteRepository
{
    public function getActive()
    {
        return Website::where('is_enabled', true)->rememberForever('v_websites')->with('logs')->orderBy('url')->get();
    }

    public function getForSSLCheck()
    {
        return Website::where('is_enabled', true)->where('check_ssl', true)->with('logs')->rememberForever('vssl_websites')->get();
    }
}
