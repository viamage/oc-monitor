<?php namespace Viamage\WebMonitor\Components;

use Backend\Facades\BackendAuth;
use Backend\Models\User;
use Cms\Classes\ComponentBase;
use Illuminate\Support\Facades\Redirect;

class CheckAdmin extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'CheckAdmin Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [
            'page' => [
                'title'       => 'Monitor Page',
                'description' => 'Monitor Page',
                'default'     => '/monitor',
            ],
        ];
    }


    public function onRun()
    {
        /** @var User $user */
        $user = BackendAuth::getUser();
        if($user && ($user->is_superuser || $user->hasAccess('viamage.webmonitor.frontend'))){
            if($this->page->url === $this->property('page')){
                return;
            }

            return Redirect::to($this->property('page'));
        }
        if($this->page->url !== '/') {
            return Redirect::to('/');
        }
    }

}
