<?php namespace Viamage\WebMonitor\Components;

use Cms\Classes\ComponentBase;
use Illuminate\Support\Facades\App;
use Viamage\WebMonitor\Contracts\WebsiteRepository;
use Viamage\WebMonitor\Repositories\WebsiteEloquentRepository;

class Monitor extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'Monitor Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun()
    {
        /** @var WebsiteEloquentRepository $repo */
        $repo = App::make(WebsiteRepository::class);

        $this->page['websites'] = $repo->getActive();
    }
}
