<?php namespace Viamage\WebMonitor\Controllers;

use Backend\Facades\Backend;
use BackendMenu;
use Backend\Classes\Controller;
use Flash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Lang;
use Viamage\WebMonitor\Classes\WebsiteChecker;
use Viamage\WebMonitor\Models\Website;

/**
 * Websites Back-end Controller
 */
class Websites extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController',
        'Backend.Behaviors.RelationController',
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';
    public $relationConfig = 'config_relations.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Viamage.WebMonitor', 'webmonitor', 'websites');
    }

    /**
     * Deleted checked websites.
     */
    public function index_onDelete()
    {
        if (($checkedIds = post('checked')) && is_array($checkedIds) && count($checkedIds)) {

            foreach ($checkedIds as $websiteId) {
                if (!$website = Website::find($websiteId)) {
                    continue;
                }
                $website->delete();
            }

            Flash::success(Lang::get('viamage.webmonitor::lang.websites.delete_selected_success'));
        } else {
            Flash::error(Lang::get('viamage.webmonitor::lang.websites.delete_selected_empty'));
        }

        return $this->listRefresh();
    }

    public function onRefreshWebsite(){
        $id = $this->params[0];
        $website = Website::where('id', $id)->first();
        $checker = new WebsiteChecker($website);
        $checker->check();
        \Flash::success('Refreshed properly');
        return Redirect::to(Backend::url('viamage/webmonitor/websites/update/'.$id));
    }
}
