<?php namespace Viamage\WebMonitor;

use Backend;
use Illuminate\Console\Scheduling\Schedule;
use System\Classes\PluginBase;
use Viamage\WebMonitor\Classes\CertificateChecker;
use Viamage\WebMonitor\Classes\WebsiteChecker;
use Viamage\WebMonitor\Components\CheckAdmin;
use Viamage\WebMonitor\Components\Monitor;
use Viamage\WebMonitor\Console\Check;
use Viamage\WebMonitor\Console\SslCheck;
use Viamage\WebMonitor\Contracts\WebsiteRepository;
use Viamage\WebMonitor\Models\Settings;
use Viamage\WebMonitor\Repositories\WebsiteEloquentRepository;

/**
 * WebMonitor Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'WebMonitor',
            'description' => 'No description provided yet...',
            'author'      => 'Viamage',
            'icon'        => 'icon-leaf',
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {
        $this->registerRepositories();
        $this->commands(
            [
                Check::class,
                SslCheck::class,
            ]
        );

    }

    /**
     * @param Schedule $schedule
     */
    public function registerSchedule($schedule)
    {
        $this->registerRepositories();
        /** @var WebsiteEloquentRepository $repo */
        $repo = app()->make(WebsiteRepository::class);
        foreach ($repo->getActive() as $website) {
if($website->interval == 1){
  $cron = '* * * * *';
} else {
  $cron = '*/'.$website->interval.' * * * *';
}
            $schedule->call(
                static function () use ($website) {
                    (new WebsiteChecker($website))->check();
                }
            )->cron($cron);
        }

        $schedule->call(
            static function () use ($repo) {
                foreach ($repo->getForSSLCheck() as $website) {
                    (new CertificateChecker($website))->check();
                }
            }
        )->dailyAt('08:00');
    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {

    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return [
            Monitor::class    => 'v_monitor',
            CheckAdmin::class => 'v_checkadmin',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return []; // Remove this line to activate

        return [
            'viamage.webmonitor.some_permission' => [
                'tab'   => 'WebMonitor',
                'label' => 'Some permission',
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        return [
            'webmonitor' => [
                'label'       => 'WebMonitor',
                'url'         => Backend::url('viamage/webmonitor/websites'),
                'icon'        => 'icon-tasks',
                'permissions' => ['viamage.webmonitor.*'],
                'order'       => 500,
            ],
        ];
    }

    private function registerRepositories()
    {
        $this->app->bind(WebsiteRepository::class, WebsiteEloquentRepository::class);
    }

    /**
     * @return array
     */
    public function registerSettings()
    {
        return [
            'config' => [
                'label'       => 'WebMonitor Settings',
                'icon'        => 'icon-tasks',
                'description' => 'Configure WebMonitor',
                'class'       => Settings::class,
            ],
        ];
    }
}
