<?php

return [
    'website' => [
        'new' => 'New Website',
        'label' => 'Website',
        'create_title' => 'Create Website',
        'update_title' => 'Edit Website',
        'preview_title' => 'Preview Website',
        'list_title' => 'Manage Websites',
    ],
    'websites' => [
        'delete_selected_confirm' => 'Delete the selected Websites?',
        'menu_label' => 'Websites',
        'return_to_list' => 'Return to Websites',
        'delete_confirm' => 'Do you really want to delete this Website?',
        'delete_selected_success' => 'Successfully deleted the selected Websites.',
        'delete_selected_empty' => 'There are no selected Websites to delete.',
    ],
];