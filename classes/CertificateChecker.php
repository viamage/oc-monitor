<?php

namespace Viamage\WebMonitor\Classes;

use Keios\SlackNotifications\Classes\SlackMessageSender;
use Viamage\WebMonitor\Models\Settings;
use Viamage\WebMonitor\Models\Website;
use Spatie\SslCertificate\SslCertificate;

class CertificateChecker
{
    private $website;

    private $settings;

    public function __construct(Website $website)
    {
        $this->website = $website;
        $this->settings = Settings::instance();
    }

    public function check(): int
    {
        $certificate = SslCertificate::createForHostName(str_replace(['http://', 'https://'], '', $this->website->url));
        $days = $certificate->expirationDate()->diffInDays(); // returns an int
        $this->website->cert_days = $days;
        $this->website->save();
        if ($days < 7 && $this->settings->enable_notifications) {
            $slackSender = new SlackMessageSender();
            $slackSender->send($this->website->url.' SSL Certificate will expire in '.$days.' days! ' . $this->settings->attach_name);
        }

        return $days;
    }
}
