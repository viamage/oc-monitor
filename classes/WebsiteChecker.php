<?php

namespace Viamage\WebMonitor\Classes;

use Carbon\Carbon;
use Keios\Apparatus\Classes\RequestSender;
use Keios\SlackNotifications\Classes\SlackMessageSender;
use Viamage\WebMonitor\Models\Settings;
use Viamage\WebMonitor\Models\Website;
use Viamage\WebMonitor\Models\WebsiteLog;

class WebsiteChecker
{
    private $website;
    private $requestSender;
    private $settings;

    public function __construct(Website $website)
    {
        $this->website = $website;
        $this->requestSender = new RequestSender();
        $this->settings = Settings::instance();
    }

    public function check(): bool
    {
        $response = $this->requestSender->sendGetRequest([], $this->website->url);
        $code = $response['code'];
        if ($this->isSuccessCode($code)) {
            $this->processSuccess($code);

            return true;
        }

        $this->processFail($code, $response['error']);

        return false;
    }

    private function isSuccessCode(int $code): bool
    {
        return in_array($code, [200, 201, 301, 302], true);
    }

    private function processFail(int $code, $error)
    {
        $lastLog = $this->website->logs->sortByDesc('created_at')->first();
        if (!$lastLog || $lastLog->is_up) {
            $log = new WebsiteLog();
            $log->website_id = $this->website->id;
            $log->is_up = false;
            $log->code = $code;
            $log->error = $error;
            $log->save();
            if ($this->settings->enable_notifications) {
                $slackSender = new SlackMessageSender();
                $slackSender->send(
                    $this->website->url.' is down! Error code is '.$code.' '.$this->settings->attach_name,
                    $this->website->custom_hook
                );
            }
        }
    }

    private function processSuccess(int $code)
    {
        $lastLog = $this->website->logs->sortByDesc('created_at')->first();
        if ($lastLog && !$lastLog->is_up) {
            $log = new WebsiteLog();
            $log->website_id = $this->website->id;
            $log->is_up = true;
            $log->code = $code;
            $log->save();
            if ($this->settings->enable_notifications) {
                $slackSender = new SlackMessageSender();
                $slackSender->send(
                    $this->website->url.' is back up! It was down for '.$this->website->getDowntimeSince(
                        Carbon::now()
                    ).' minutes. '.$this->settings->attach_name,
                    $this->website->custom_hook
                );
            }
        }
        if (!$lastLog) {
            $log = new WebsiteLog();
            $log->website_id = $this->website->id;
            $log->is_up = true;
            $log->code = $code;
            $log->save();
        }
    }
}
